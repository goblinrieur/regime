set xlabel "temps"
set ylabel "poid"
set grid ls 10
set sample 800
set datafile separator "|"
set yrange [80:115]
set ytics 5
set autoscale
set title "SUIVI REGIME : historique jusqu au ".strftime("%d %b %Y", time(0)) offset 0, 1 
set xdata time
set timefmt "%Y-%m-%d"
set format x "%d-%m-%Y"
set terminal png background rgb 'grey' size 1800,1080
set output '/dev/shm/poid.png'
plot "/dev/shm/poid" using 1:2 with lines smooth bezier title "tendance Regime" , "/dev/shm/poid" using 1:2 with lines lt rgb "red" title "Poid reel", "/dev/shm/objectif" using 1:2 with lines smooth bezier lt rgb "white" title "Objectif"
