# Régime

script pour le fun pour générer une image gnu-plot avec les courbes de 

suivi de progression du régime, _(ce dernier n'est pas forcément suivi avec grand sérieux)_

# Choix ?

le script se base sur un script ansible pour les modules de vérifications de pré-requis

sur un script gnuplot pour la partie build graph

sur postgreql comme bdd puisque ma table de suivi est là, en plus c'est pratique pour

faire des appels via ansible, et enfin sur bc pour pour faire un petit calcul de temps.

Je m'envoie enfin par mail le graph en png comme pièce jointe via mutt.

## Ce script n'existe que pour le fun 

reprenez le / adaptez le / changez le 

_attention beaucoup d'éléments sont en dur au lieu d'être en variables_

faites en ce que vous voulez 

## Liens vers les sous-scripts

[gnuplot](https://gitlab.com/goblinrieur/regime/-/tree/master/vars/regime.gnuplot)

[yaml](https://gitlab.com/goblinrieur/regime/-/tree/master/yaml)

[go.sh](https://gitlab.com/goblinrieur/regime/-/tree/master/go.sh)

## Exemple de courbe de résultat

![poid.png](poid.png)

presque 1 moi sans mesures, voyons ce que ça donne

## exemple d'exécution du script sur ma config :

![screen.png](history/screen.png)

## Me contacter 

on peut aussi me contacter sur : [goblinrieur@gmail.com](mailto://goblinrieur@gmail.com)

ou on peut me joindre sur discord : [discord](https://discord.gg/9szvduB)

ou via la chaîne youtube : [youtube](https://www.youtube.com/channel/UCnsW_UH9vXX-nBe3X-enXYA/)

# Régime

## Conseils généraux de régime lent & pas trop dur à maintenir/supporter:

_Assez facile à tenir sur le long terme à condition de noter son avancée dans la table de suivi_

### Chaque semaine :
- 1x / semaine :  régime végétarien

- 2x _si raisonnable_ sinon 1x / semaine :  cheatmeal _(pizza OU fast-food OU autre)_  *mais toujours le midi*

- tous les soirs : léger _(seulement plat ou seulement entrée ou seulement dessert (si léger))_

- tous les midis : manger normalement _juste en surveillant un peu, enlever le gras de jambon/peau de poulet, pas de fritures_

- tous les matins : faire l'effort de manger un petit quelque chose en plus du café/thé _(et surtout le prendre avant 8H30)_

- 1x ou 2x par mois : faire un jeune sec de 24H _(ou un jeune alimentaire si c'est trop dur)_

### En complément
- essayer de ne pas faire de grasses mat le W.E. au delà de 10H 

- efforts physiques : 

	- marcher 6000 pas 2x ou 3x dans la semaines

	- plus 10 pompes chaque jours et gainage 1jours / 2 

 _(moyenne)_ suffisent

- suivre le régime dans une table SQL est un moyen simple de le faire dans mon cas j'utilise postgresql parce que j'ai pleins d'autres sujets.

- un simple sqlite peut suffire , voir même un tableau papier

### le modèle de table que j'utilise :

```
                                                            Table "public.regime"
       Column        |       Type       | Collation | Nullable |               Default               | Storage  | Stats target | Description
---------------------+------------------+-----------+----------+-------------------------------------+----------+--------------+-------------
 ref                 | integer          |           | not null | nextval('regime_ref_seq'::regclass) | plain    |              |
 date                | date             |           |          |                                     | plain    |              |
 poid                | double precision |           |          |                                     | plain    |              |
 shot                | integer          |           |          |                                     | plain    |              |
 bruleur             | integer          |           |          |                                     | plain    |              |
 bares               | integer          |           |          |                                     | plain    |              |
 infusions           | integer          |           |          |                                     | plain    |              |
 complement          | integer          |           |          |                                     | plain    |              |
 supplementvitamines | boolean          |           |          |                                     | plain    |              |
 commentaire         | text             |           |          |                                     | extended |              |
 substitus           | integer          |           |          |                                     | plain    |              |
 objectif            | integer          |           |          |                                     | plain    |              |
 Indexes:
    "regime_pkey" PRIMARY KEY, btree (ref)
    "constraintdate" UNIQUE CONSTRAINT, btree (date)
```

### exemple d' utilisation : 
En temps réel ça ressemble à ceci : 

```
francois=# select * from regime where date >= current_date -3 ;                                                                                                                              
 ref |    date    | poid | shot | bruleur | bares | infusions | complement | supplementvitamines |             commentaire              | substitus | objectif                            
-----+------------+------+------+---------+-------+-----------+------------+---------------------+--------------------------------------+-----------+----------                           
  55 | 2020-08-31 | 96.6 |    3 |      12 |     0 |        11 |          0 | f                   | bon ca va                            |         0 |       97                            
  56 | 2020-09-01 | 95.5 |    2 |       7 |     0 |        11 |          0 | f                   | apres une 1er tentative de jeune 20h |         0 |       95                            
  57 | 2020-09-02 | 96.5 |    1 |       2 |     0 |        11 |          0 | f                   | le cheatmeal a ete trop FAT          |         0 |       95                            
  58 | 2020-09-03 | 97.2 |   12 |     117 |     0 |        11 |        120 | t                   | repprovisionnement produits          |         0 |       95                            
(4 rows)
francois=#
```

les insertions se font avec une requête très simple ; je vous met un exemple ci dessous :

```
francois=# insert into regime values ((select max(ref)+1 from regime),current_date,96.6,3,17-5,0,11,0,false,'bon ca va',0,97);
```


## Conseils pour régime rapide 
_(plus traumatisant pour le corps), un peu plus cher_

### Le Régime :
**attention : ce genre de régime est plus traumatisant pour le corps et plus dur à tenir mentalement nécessite**

**beaucoup d'efforts ; il n'est pas conseillé si vous avez tendance à la dépression par exemple**

- 1x / semaine : régime végétarien + complément vitaminés + barre

- 1x / semaine : Cheatmeal obligatoirement le midi pour pouvoir se dépenser un peu

- tous les soirs : barre de substitution _ou_ repas hyper léger avec un complément non-vitaminé + brûleur de graisse

- tous les midis : manger normalement + complément vitaminé pour tenir la journée de ce régime fatiguant

- tous les matins : **obligation** de petit déjeuner avec un solide + thé/café + complément vitaminé + brûleur de graisse

- 1x ou 2x par mois : faire un jeune sec de 24H

accordez vous 1 cheatmeal à midi *_obligatoirement_* tous les 10 à 15 jours 

essayez de ne pas faire de grasses mat le W.E. au delà de 9H00 

### Le complément physique : 

**efforts physiques :**
	

- marcher 6000 pas 2x ou 3x dans la semaines _(ou mieux)_

- 10 pompes chaque jour ouvrable _(réveil musculaire au saut du lit avant la toilette et le petit dej)_

- 10 minutes de gainage  _(à la suite si possible sinon le soir avant le dîner)_

- 40 minutes d'haltères 2 à 3 x par semaine _(attention à ne pas se blesser, espacer les séances et respirez aux bon moment)_

**Si vous préférez un sport collectif, ou individuel en club, procédez avec le même calendrier**
