CREATE OR REPLACE FUNCTION insert_into_poid()
RETURNS TRIGGER AS $$
BEGIN
    -- Insert the new row into poid
    INSERT INTO poid (ref, date)
    VALUES (NEW.ref, NEW.date)
    ON CONFLICT (ref, date) DO NOTHING;  -- Avoid inserting duplicates
    
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER after_regime_insert
AFTER INSERT ON regime
FOR EACH ROW
EXECUTE FUNCTION insert_into_poid();


ALTER TABLE poid ADD CONSTRAINT poid_ref_date_unique UNIQUE(ref, date);
ALTER TABLE regime ADD CONSTRAINT regime_ref_date_unique UNIQUE(ref, date);

