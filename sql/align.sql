UPDATE poid
SET ref = (
    SELECT t2.ref
    FROM regime t2
    WHERE poid.date = t2.date
)
WHERE EXISTS (
    SELECT 1
    FROM regime t2
    WHERE poid.date = t2.date
    AND poid.ref <> t2.ref
)
AND NOT EXISTS (
    SELECT 1
    FROM poid p
    WHERE p.ref = (
        SELECT t2.ref
        FROM regime t2
        WHERE poid.date = t2.date
    )
);
