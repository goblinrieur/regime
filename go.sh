#! /bin/bash

function intro(){
	clear
	echo -e "\n\n\n\t\t running the \e[31m\e[1m$0\e[0m script version \e[31m\e[1m$(git rev-parse --short HEAD)\e[0m git branch \e[31m\e[1m$(git rev-parse --abbrev-ref HEAD)\e[0m\n\n\n\n\n"
	mv poid.png previouspoid.png
    return 0
}

function can_it_run(){
	echo check script can call playbook
	test -f /usr/bin/ansible-playbook || ( echo /usr/bin/ansible-playbook introuvable ; exit 1 ) 
	test -f inventory/inventory || ( echo inventory/inventory introuvable ; exit 1 ) 
	test -f yaml/psql.yaml || ( echo yaml/psql.yaml introuvable ; exit 1 ) 
	test -f /usr/bin/mutt || ( echo  /usr/bin/mutt introuvable ; exit 1 ) 
        return 0
}

function go(){
	ansible-playbook  -i inventory/inventory yaml/psql.yaml 
	mv /dev/shm/poid.png . 2> /dev/null || ( echo -e "\e[31m\e[1mFailed to retrieve picture\e[0m\n\n\n " ; exit 1 )
        return 0
}


start_time="$(date -u +%s.%N)"
intro   || exit 1
can_it_run
go
end_time="$(date -u +%s.%N)"
echo -e "\t\tscript ends after \e[31m\e[1m "$(bc <<<"$end_time - $start_time")"s\e[0m\n\n\n" 
unset start_time
unset end_time
git add -A ; git commit -m "$(date +"%y%m%d")" && git push 
exit 0
